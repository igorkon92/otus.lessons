using System;

namespace ConsoleApp
{
    /// <summary>
    /// Бойцы
    /// </summary>
    public class Fighters
    {
        public MmaFighter[] fighters;

        /// <summary>
        /// Конструктор
        /// </summary>
        public Fighters()
        {
            fighters = new MmaFighter[3];
        }

        /// <summary>
        /// Проведение боя
        /// </summary>
        /// <param name="fighter1">Боец 1</param>
        /// <param name="fighter2">Боец 2</param>
        public void Fight(MmaFighter fighter1, MmaFighter fighter2)
        {
            // использование StringExtension
            Console.WriteLine($">>> {fighter1.GetName.FirstWord()} vs {fighter2.GetName.FirstWord()}");
            var winner = fighter1 / fighter2;
            Console.WriteLine($" $$$ Winner - {winner.GetName}");
        }

        /// <summary>
        /// Поиск более сильного бойца
        /// </summary>
        /// <param name="fighter"></param>
        /// <returns></returns>
        public MmaFighter GetMorePowerfullFighter(MmaFighter fighter)
        {
            foreach (var mmaFighter in fighters)
            {
                if (fighter.GetStatePower < mmaFighter.GetStatePower)
                {
                    return mmaFighter;
                }
            }
            return null;
        }
        
        /// <summary>
        /// Индексатор 
        /// </summary>
        /// <param name="index"></param>
        public MmaFighter this[int index]
        {
            get
            {
                return fighters[index];
            }
            set
            {
                fighters[index] = value;
            }
        }
    }
}