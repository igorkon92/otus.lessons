namespace ConsoleApp
{
    /// <summary>
    /// Расширение класса String
    /// </summary>
    public static class StringExtensions
    {
        public static string FirstWord(this string str) => str.Split(" ")[0];
    }
}