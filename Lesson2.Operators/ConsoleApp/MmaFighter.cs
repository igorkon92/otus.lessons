using System;

namespace ConsoleApp
{
    /// <summary>
    /// Боец ММА
    /// </summary>
    public class MmaFighter
    {
        /// <summary>
        /// Имя бойца
        /// </summary>
        private string _name;

        /// <summary>
        /// Число боев
        /// </summary>
        private int _fights;

        /// <summary>
        /// Сила бойца
        /// </summary>
        private double _power;

        /// <summary>
        /// Число побед
        /// </summary>
        private int _wins;

        /// <summary>
        /// Получение имени бойца
        /// </summary>
        public string GetName => _name;
        
        /// <summary>
        /// Получение текущей силы бойца
        /// </summary>
        public double GetStatePower => _power;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">имя бойца</param>
        /// <param name="power">сила бойца</param>
        public MmaFighter(string name, double power)
        {
            _name = name;
            _fights = 0;
            _power = power;
            _wins = 0;
        }

        /// <summary>
        /// Восстановление
        /// </summary>
        /// <param name="days">число дней</param>
        public void Recovery(int days)
        {
            _power += days * 0.2;
        }

        

        /// <summary>
        /// Расширение метода toString
        /// </summary>
        /// <returns>Боец - {имя бойца}; Проведено боев - {число боев}; Побед - {число побед}</returns>
        public override string ToString()
        {
            return $"Боец - {_name}; Проведено боев - {_fights}; Побед - {_wins}";
        }
        
        /// <summary>
        /// Расширение оператора / - Проведение боя
        /// </summary>
        /// <param name="fighter1">Боец1</param>
        /// <param name="fighter2">Боец2</param>
        /// <returns>Победитель</returns>
        public static MmaFighter operator / (MmaFighter fighter1, MmaFighter fighter2)
        {
            fighter1._fights++;
            fighter2._fights++;
            // вычисляем победителя по силе
            MmaFighter winner = (fighter1.GetStatePower > fighter2.GetStatePower) ? fighter1 : fighter2;
            winner._wins++;
            winner._power -= 3;
            return winner;
        }
        
        /// <summary>
        /// Расширение оператора / - поиск более сильного бойца
        /// </summary>
        /// <param name="fighters">Бойцы</param>
        /// <param name="fighter">боец</param>
        /// <returns></returns>
        public static MmaFighter operator / (Fighters fighters, MmaFighter fighter)
        {
            return fighters.GetMorePowerfullFighter(fighter);
        }
    }
}