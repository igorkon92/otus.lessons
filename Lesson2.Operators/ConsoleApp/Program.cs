﻿using System;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Lady's and gentls'!");
            
            Fighters fighters = new Fighters();
            
            // инициализация бойцов
            fighters[0] = new MmaFighter("Khabib Nurmagomedov", 10);
            fighters[1] = new MmaFighter("Konor MacGregor", 8);
            fighters[2] = new MmaFighter("Duncan Ferguson", 9);
            
            Console.WriteLine("--Start tournament--");
            TournamentState(fighters);
            
            // первй день
            Console.WriteLine("-Day 1-");
            fighters.Fight(fighters[0], fighters[1]); // проведение боя
            fighters[2].Recovery(1);// востановление бойца, не участвовашего в бою
            TournamentState(fighters); // вывод состояния
            
            Console.WriteLine("-Day 2-");
            fighters.Fight(fighters[2], fighters[1]);
            fighters[0].Recovery(1);
            TournamentState(fighters);
            
            Console.WriteLine("-Day 3-");
            fighters.Fight(fighters[2], fighters[0]);
            fighters[1].Recovery(1);
            TournamentState(fighters);
            
            Console.WriteLine($"Who is more powerfull that {fighters[0]} in now?");
            Console.WriteLine((fighters / fighters[0]).GetName.FirstWord()); // использование расширенного оператора /
        }

        /// <summary>
        /// Состояние турнира
        /// </summary>
        /// <param name="fighters"></param>
        private static void TournamentState(Fighters fighters)
        {
            foreach (var fighter in fighters.fighters)
            {
                Console.WriteLine(fighter);
            }
        }
    }
}